#include <iostream>
#include "MySharedPtr.h"

using namespace std;
struct Apple{
    Apple(){
        cout << "Apple constructor\n";
    }
    ~ Apple(){
        cout << "Apple destructor\n";
    }
    void debug(){
        cout <<" I'm apple\n";
    }
} ; 

class Object
{
public:
    Object(){
        cout << "Object constructor\n";
    }
    ~Object(){
        cout << "Object destructor\n";
    }
    Object(const Object &other){
        cout << "Object copy\n";
    }
    
    Object& operator= (const Object &other){
        
        cout << "Object copy\n";
        return *this;
    }

    Object(Object &&other){
        cout << "Object move copy\n";
    }

    Object& operator= ( Object &&other){
        
        cout << "Object move =\n";
        return *this;
    }
    void debug(){
        cout <<"Object print\n";
    }
};

unique_ptr<Object> func(unique_ptr<Object> obj){
    obj->debug();
    return obj;
}
#include <memory>
void run_mysharedptr(){

/*    MySharedPtr<Apple> ptr(new Apple());    
    
    MySharedPtr<Apple> ptr2(ptr);
    MySharedPtr<Apple> ptr3(ptr2);
    ptr3=ptr2;
    cout<<"ptr2.use_count:"<< ptr2.use_count()<<endl;
    ptr3->debug();

    MySharedPtr<Apple> var1(new Apple());
    ptr3 = var1;
 */
    //unique_ptr<Object> p = make_unique<Object>();
    Object a;
    Object b =(Object&&)a;
    //func(std::move(p));
    //cout << "return main\n";
    //  p2->debug();
}
