#ifndef __Configure__
#define __Configure__
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
using namespace std;
class Configure{
public:
    Configure(const string & filename) : m_errMsg(""){
        ifstream iniFile;
        iniFile.open(filename);
        if(!iniFile.is_open()) {
            m_errMsg="unable open " + filename;
        }
        string tmp;
        while(getline(iniFile, tmp)) {
            if(tmp.length() == 0 || tmp.at(0) == '#')
                continue;
            string::size_type n = tmp.find('=');
            if (n != string::npos ){
                m_pairs[ tmp.substr(0,n) ] = tmp.substr(n+1) ;
            }

        }
    }
 public:

    bool isGood(){
        return m_errMsg.length() == 0;
    }

    const string& getErrMsg(){
        return m_errMsg;
    }

    const string* getValue( const string & key){
        unordered_map<string, string>::iterator itr = m_pairs.find(key);
        if (  itr!= m_pairs.end() ){
            return &(itr->second);
        }else{
            return nullptr;
        }
    }
    void dump(){
        for(auto item: m_pairs){
            cout<< item.first<< "=" <<item.second <<endl;
        }
    }
private:
    string m_errMsg;
    unordered_map<string, string> m_pairs; 
};


#endif