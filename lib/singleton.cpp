#include "singleton.h"
#include <iostream>

//using namespace singleton;


int Singleton::count=0;
Singleton * Singleton::m_pSingleton = nullptr;

pthread_mutex_t Singleton::lock; 

void ut_singleton(){

    Singleton *pSingleton, *pSingleton2;
    for(int i=0; i < 10000; ++i){
        pSingleton = Singleton::getInstance();
        std::cout << "first call: " <<  pSingleton->getCount() << std::endl;

        pSingleton2 = Singleton::getInstance();
        std::cout << "sec call: " <<  pSingleton2->getCount()  << std::endl;
    }
    pSingleton->release();
    pSingleton2->release();




}
