
#include "iterator.h"
template<typename T>
_MyContainterItr<T>::_MyContainterItr(MyContainter<T> * powner,  T *pt) {
    m_pointer=pt;
}

template<typename T>
_MyContainterItr<T>& _MyContainterItr<T>::operator++(){
    
    m_pointer++;
    return *this;
} 

template<typename T>
_MyContainterItr<T>& _MyContainterItr<T>::operator--(){
    m_pointer--; 
    return *this;
}
template<typename T>
T& _MyContainterItr<T>::operator *(){
    return *m_pointer;
}

template<typename T>
MyContainter<T>::MyContainter(size_t size):m_maxsize(size), m_currpos(0){
    m_membuff = new T[m_maxsize];
}

template<typename T>
MyContainter<T>::~MyContainter(){
    delete []m_membuff;
}

template<typename T>
int MyContainter<T>::push_back(T &&val){
    if(m_currpos == m_maxsize){
        return -1;
    }
    m_membuff[m_currpos] = val;
    m_currpos++;
    return m_currpos;
}
#include <vector>
void test_myiterator(){
    MyContainter<char> arr(26);
    for( int i=0; i < 26 ; ++i){
        arr.push_back('a' +i);
    }

    MyContainter<char>::iterator itr = arr.begin();
    MyContainter<char>::iterator end = arr.end();
    while(itr != end){
        std::cout << *(itr) << std::endl;
        ++itr;
    }

    std::vector<int> vv;
    vv.begin();
}
