#ifndef __UTILS__H
#define __UTILS__H

namespace StringUtil
{
    void parserStr(std::string s, std::vector<std::string>& v, std::string delims){
        int nextPos=0;
        int currPos=0;
        std::string tmp;
        while ( (nextPos=s.find_first_of(delims, currPos)) !=s.npos){
            tmp= s.substr(currPos, nextPos - currPos);
            if ( tmp.size() >0){
            v.push_back(tmp);
            }
            currPos=nextPos+1;
        }
        v.push_back(s.substr(currPos));  
    }
}

#endif
