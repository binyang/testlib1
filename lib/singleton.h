#ifndef __SINGLETON__
#define __SINGLETON__
#include <memory>
#include <iostream>
#include <pthread.h>
class Singleton{
private:
    Singleton (){
        count++;
    }
    Singleton & operator=(const Singleton & l);
    Singleton( Singleton & l);
    Singleton( const Singleton & l);
    ~ Singleton(){
        count--;
        std::cout << "destructor\n";
    }
public:

    static Singleton* getInstance(){
        if (m_pSingleton == nullptr ){
            pthread_mutex_lock(&lock); 
            m_pSingleton = new Singleton();
            pthread_mutex_unlock(&lock); 
        }else{
            return m_pSingleton;
        }
    }
    int getCount(){ 
        return count; 
    }
    static void release(){
        pthread_mutex_lock(&lock); 
        if(m_pSingleton != nullptr){
            delete m_pSingleton;
             m_pSingleton = nullptr;
        }
        pthread_mutex_unlock(&lock); 
    }
private:

    static Singleton * m_pSingleton;
    static int count;
    static pthread_mutex_t lock; 
    

    };


    void ut_singleton();



#endif