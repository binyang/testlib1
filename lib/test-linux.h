
#ifndef TEST_LNNUX_H
#define TEST_LNNUX_H

namespace LINUX_BASICS{

void test_linux();
int get_shared_block(const char * filename, int size);
char *attach_memory_block(const char *filename, int size);
bool destroy_memory_block(const char *filename);
}
#endif

