
#ifndef __ITERATOR__
#define __ITERATOR__

#include <iostream>
#include <string.h>
template<class T> class MyContainter;


template<typename T>
class _MyContainterItr{
public:
    _MyContainterItr(MyContainter<T> * powner, T *pt);

    _MyContainterItr& operator++();
    _MyContainterItr& operator--();

    T& operator *();

    bool operator != (_MyContainterItr<T> & other){
        if (this == &other)
            return false;
        return m_pointer != other.m_pointer ;
    }

    bool operator == (_MyContainterItr<T> & other){
        if (this == &other)
            return true;
        return m_pointer == other.m_pointer ;
    }
 private:   
    T *m_pointer;
};



template<typename T>
class MyContainter{

public:
    MyContainter(size_t size);
    ~MyContainter();
    using iterator = _MyContainterItr<T> ;
    friend class _MyContainterItr<T>;
    int push_back(T && val);
    iterator begin(){
        return _MyContainterItr<T>(this, m_membuff);   
    }
    iterator end(){
        return _MyContainterItr<T>(this, m_membuff + m_maxsize ); 
    }
    
private:
    MyContainter(const MyContainter & rhs);
    MyContainter& operator=(const MyContainter & rhs);
private:
    
    int m_maxsize;
    int m_currpos;
    T *m_membuff;

};

void test_myiterator();

#endif
