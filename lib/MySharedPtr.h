#ifndef __MYSHARED_PTR__
#define __MYSHARED_PTR__
#include <iostream>
#include<pthread.h>
using namespace  std;


template< typename T>
class MySharedPtr{
public:
    MySharedPtr(T * pobj):m_pObj(pobj){
        m_pcount = new int(1);
        m_plock = new pthread_mutex_t();
        if (pthread_mutex_init(m_plock, NULL) != 0){
            cout <<  " mutex init failed\n";
        }
      
    }
    MySharedPtr(const MySharedPtr & rhs){
        if(this != &rhs){
            m_pcount = rhs.m_pcount;
            m_pObj = rhs.m_pObj;
            incmentCount();
        }
    }
    const MySharedPtr& operator=(const MySharedPtr & rhs){
        if(this != &rhs){
            pthread_mutex_lock(m_plock);
            if(this->m_pObj != nullptr){
                 (*m_pcount)--;
                if(!*m_pcount){
                    delete m_pObj;
                    delete m_pcount;
                    m_pObj = nullptr;
                    m_pcount= nullptr;
                }
            }
            m_pcount = rhs.m_pcount;
            m_pObj = rhs.m_pObj;
            (*m_pcount)++;
            pthread_mutex_unlock(m_plock);
        }
        return *this;
        
    } 
    ~MySharedPtr(){
        pthread_mutex_lock(m_plock);
        (*m_pcount)--;
        if(! *m_pcount){
            delete m_pObj;
            delete m_pcount;
            m_pObj = nullptr;
            m_pcount= nullptr;
        }
        pthread_mutex_unlock(m_plock);
    }
    int use_count() const{
        return *m_pcount;
    }
    T * operator->(){
        return m_pObj;
    }
private:
    void decmentCount() const {
        pthread_mutex_lock(m_plock);
        (*m_pcount)--;
        pthread_mutex_unlock(m_plock);
    }   
    void incmentCount() const {
        pthread_mutex_lock(m_plock);
        (*m_pcount)++;
        pthread_mutex_unlock(m_plock);
    } 
 
private:
    pthread_mutex_t *m_plock;
    int * m_pcount;
    T *m_pObj;    
};



void run_mysharedptr();


#endif