#include <sys/types.h>
#include <sys/ipc.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <string.h>
#include <semaphore.h>
#include "test-linux.h"

namespace LINUX_BASICS
{
#define IPC_RESULT_ERROR -1

int get_shared_block(const char * filename, int size){
    key_t key;
    key = ftok(filename, 0);
    if(key == IPC_RESULT_ERROR){
        printf("ftok failed, error:%s\n", strerror(key));
        return IPC_RESULT_ERROR;
    }
    return shmget(key, size, 0644 | IPC_CREAT);
}

char *attach_memory_block(const char *filename, int size){
    int shared_block_id  = get_shared_block(filename, size);
    if(shared_block_id == IPC_RESULT_ERROR){
        return NULL;
    }

    char* result = (char*)shmat(shared_block_id, NULL, 0);
    if( result ==(char*)-1){
        return NULL;
    }
    return result;
}

bool destroy_memory_block(const char *filename){
    int shared_block_id  = get_shared_block(filename, 0);
    if(shared_block_id == IPC_RESULT_ERROR){
        return NULL;
    }
    return (shmctl(shared_block_id, IPC_RMID, NULL) != -1);
}


void test_linux(){
    int ret = get_shared_block("shmfile.shm", 4096);

    printf("ret:%d", ret);

}



sem_t *prod_mutex; 
sem_t *cons_mutex; 

//this consumer function is called in process A or thread A
void test_semaphore_A(){

  sem_unlink("/tmp/cons");
  sem_unlink("/tmp/prod");
  cons_mutex = sem_open ("/tmp/cons", O_CREAT | O_EXCL, 0644, 0);
  prod_mutex = sem_open ("/tmp/prod", O_CREAT | O_EXCL, 0644, 1);
 

  char * ptr = LINUX_BASICS::attach_memory_block("/tmp/shm", 4096);
  while(true){
    printf("wait cons_mutex\n");
    sem_wait(cons_mutex);
    printf("%s\n", ptr);
    if( strcmp(ptr, "quit")==0){
      break;
    }
    sem_post(prod_mutex);
    printf("post prod_mutex\n");
  }

  LINUX_BASICS::destroy_memory_block("/tmp/shm");
  sem_destroy(cons_mutex);
  sem_destroy(prod_mutex);

}

//this producer function is called in process B or thread B 
void test_semaphore_B()
{

  cons_mutex = sem_open ("/tmp/cons",  0);
  prod_mutex = sem_open ("/tmp/prod",  1);

  char * ptr = LINUX_BASICS::attach_memory_block("/tmp/shm", 4096);
  while(true){
    printf("wait prod_mutex\n");
    sem_wait(prod_mutex);
    static int g_num=0;
    if(g_num==100){
      sprintf(ptr, "quit");  
    }else{
      sprintf(ptr, "producer write %d", g_num);
      printf("producer write %d\n", g_num);
      g_num++;
    }
    sem_post(cons_mutex);
    printf("post cons_mutex\n");
  }

  LINUX_BASICS::destroy_memory_block("/tmp/shm");
  sem_destroy(cons_mutex);
  sem_destroy(prod_mutex);


}



}



