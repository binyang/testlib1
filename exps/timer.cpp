


#include <stdio.h>
#include <vector>
#include <thread>
#include <chrono>
#include <iostream>
#include "timer.h"



using namespace std;
class Timer{
public:
    Timer(const char* name):m_name(name){
        m_startTime = std::chrono::high_resolution_clock::now();
    }
    ~Timer(){
        auto endTime = std::chrono::high_resolution_clock::now();
        chrono::duration<double> diff = endTime - m_startTime;
        std::cout <<m_name <<" off:" << diff .count() << "\n";

    }

private:
    std::chrono::time_point<chrono::steady_clock> m_startTime;
    std::string m_name;
    

};

using CALLBACK=void(*)(std::string s, int n);

void timer1(CALLBACK func, std::string s, int n)
{
    static int loop=20;
    char pbuff[64];
    while((loop--) > 0  ){
        // simulate expensive operation
        std::this_thread::sleep_for(std::chrono::seconds(2));
        sprintf(pbuff, "%s-%d", s.c_str(), loop);
        func(pbuff, n);
    }

}
void call_back(std::string s, int n){
    for(int i=0; i< n; ++i)
        std::cout <<"timer[] ->" <<s<< "\n";
}

void test_timer(){
    std::string t="hello, world";
    int N=5;
    std::thread th1(timer1, call_back, std::move(t), N);
    th1.join();
}
