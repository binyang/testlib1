/* Processed by ecpg (13.1) */
/* These include files are added by the preprocessor */
#include <ecpglib.h>
#include <ecpgerrno.h>
#include <sqlca.h>
/* End of automatic include section */

#line 1 "pg-ecpg.pg"
#include "ecpg.h"
/* error handlers for the whole program */
/* exec sql whenever sqlerror  call die ( ) ; */
#line 3 "pg-ecpg.pg"

/* exec sql whenever not found  break ; */
#line 4 "pg-ecpg.pg"

 
static void die(void)
{
    /* avoid recursion on error */
    /* exec sql whenever sqlerror  continue ; */
#line 9 "pg-ecpg.pg"

 
    fprintf(
        stderr,
        "database error %s:\n%s\n",
        sqlca.sqlstate,
        sqlca.sqlerrm.sqlerrmc
    );
 
    { ECPGtrans(__LINE__, NULL, "rollback");}
#line 18 "pg-ecpg.pg"

    { ECPGdisconnect(__LINE__, "CURRENT");}
#line 19 "pg-ecpg.pg"

 
    exit(1);
 
    /* restore the original handler */
    /* exec sql whenever sqlerror  call die ( ) ; */
#line 24 "pg-ecpg.pg"

}

  void run_ecpg(){

   /* exec sql begin declare section */
      
     
     
    
#line 30 "pg-ecpg.pg"
 int v_val_ind ;
 
#line 31 "pg-ecpg.pg"
 char v_key [ 64 ] ;
 
#line 32 "pg-ecpg.pg"
 char v_val [ 64 ] ;
/* exec sql end declare section */
#line 33 "pg-ecpg.pg"

 
    /* declare c cursor for select user_id , prod_id from member . user limit 1000 */
#line 37 "pg-ecpg.pg"

        
 
    /* connect to the database */
    { ECPGconnect(__LINE__, 0, "tcp:postgresql://membership-aurora.usr.int.pib.dowjones.io:5432/bas_11182020" , "mbrshp" , "member!01" , "myconnection", 0); 
#line 42 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 42 "pg-ecpg.pg"


 
    /* open a cursor */
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "declare c cursor for select user_id , prod_id from member . user limit 1000", ECPGt_EOIT, ECPGt_EORT);
#line 46 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 46 "pg-ecpg.pg"

 
    /* loop will be left if the cursor is done */
    for(;;)
    {
        /* get the next result row */
        { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "fetch next from c", ECPGt_EOIT, 
	ECPGt_char,(v_key),(long)64,(long)1,(64)*sizeof(char), 
	ECPGt_NO_INDICATOR, NULL , 0L, 0L, 0L, 
	ECPGt_char,(v_val),(long)64,(long)1,(64)*sizeof(char), 
	ECPGt_int,&(v_val_ind),(long)1,(long)1,sizeof(int), ECPGt_EORT);
#line 52 "pg-ecpg.pg"

if (sqlca.sqlcode == ECPG_NOT_FOUND) break;
#line 52 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 52 "pg-ecpg.pg"

 
        printf(
            "key = %s, value = %s\n",
            v_key,
            v_val_ind ? "(null)" : v_val
        );
    }
 
    { ECPGdo(__LINE__, 0, 1, NULL, 0, ECPGst_normal, "close c", ECPGt_EOIT, ECPGt_EORT);
#line 61 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 61 "pg-ecpg.pg"

    { ECPGtrans(__LINE__, NULL, "commit");
#line 62 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 62 "pg-ecpg.pg"

    { ECPGdisconnect(__LINE__, "CURRENT");
#line 63 "pg-ecpg.pg"

if (sqlca.sqlcode < 0) die ( );}
#line 63 "pg-ecpg.pg"


  }

