#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <memory>
#include <thread>
#include <mutex>
#include <unistd.h>
#include "inf.h"
#include "pg-ecpg.h"
#include "mylib1.h"
#include "mylib2.h"
#include "test-libcnf.h"
#include "Configure.h"
#include "test-gtest.h"
#include "test-redis.h"
#include "test-boost.h"
#include "iterator.h"
#include "MySharedPtr.h"
#include "test-linux.h"

#include <atomic>



extern  int test_asio_client(int argc, char* argv[]);
extern int test_cppunit();

//1,2,3,4
//5,6,7,8
//====>>>
//5,1
//6,2
//7,3
//8,4
void  flip(const vector<vector<int>> & input, vector<vector<int>> & output){
  int m=input.size(); //m=2
  int n=input[0].size(); //n=4
  output.resize(n);
  for(int i=0; i < n;++i){
    output[i].resize(m);
  }

  for(int i=0; i< m; ++i){
    for(int j=0; j< n; ++j){
      output[j][m-i-1]= input[i][j];
    }
  }

}

/*

  class Item 
{ 
   public: 
    int * p_data; 
    Item(int n) 
    { 
          p_data=new int[n]; 
    } 
    ~Item() 
    { 
         delete [] p_data; 
    } 
}; 
std::vector<Item> v; 
*/
#include <memory>
#include <stdio.h>

std::mutex g_mtx;
void* writer(void* args){
 FILE *fhandle =(FILE*)args;
 char mem[8];
 for(int i=0; i < 10000; i++){
   sprintf(mem, "%d\n", i);
   {
    std::lock_guard<mutex> lock(g_mtx);
    fwrite(mem, strlen(mem), 1, fhandle); 
   }
 }
 //fclose(fhandle); 
}

void* reader(void* args){
 FILE *fhandle =(FILE*)args;
 char mem[8];
 int num=1000;
 while( (num--) > -1){
   {
    memset(mem, 0x0, 8);
    std::lock_guard<mutex> lock(g_mtx);
    fgets(mem, 8, fhandle);
   }
  printf("%s", mem);
 } 
 fclose(fhandle); 
}


struct AA{
  AA(int n):m_i(n){

  }
  void inr(){
    int m=0;
    while(m++ < 10000){
      std::lock_guard<mutex> lock(g_mtx);
      m_i++;
    }
  }
  int get(){
    return m_i;
  }
  int m_i;
};


template<size_t T>
void func1(){
  int arr[T];
  cout << sizeof(arr)/(sizeof(int)) <<endl;
}

void* func( shared_ptr<AA> & ptrAA ){
  ptrAA->inr();
}

typedef  void (*FUNC)();

int main(int argc, char* argv[]){
  vector<FUNC>  vFunc;
  vFunc.push_back(func1<10>);
  vFunc.push_back(func1<20>);
  vFunc.push_back(func1<30>);

  for(auto f: vFunc){
    f();
  }

}

