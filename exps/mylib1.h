
#ifndef __MYLIB1__
#define __MYLIB1__


#include <iostream>
#include "inf.h"
#include <string.h>
#include <initializer_list>
#include <chrono>

using namespace std;
class Mylib1 : public Inf{
public:
    Mylib1(){
        std::cout << "Mylib1 constructor\n"; 
    }
    virtual ~Mylib1(){
        std::cout << "Mylib1 destructor\n"; 
    }
    virtual void test(){
        std::cout << "Mylib1 address:" << this <<std::endl;
    }
    int get( int x) const {
        return x;
    }
};



template <typename T>
T square(T a ){
  return a*a;
}

template<typename T>
class YBVector{
public:
    YBVector(std::initializer_list<T>& l){
        size=0;
        for (auto itr = l.begin(); itr != l.end(); itr++){
            push( *itr );
        }
    }
    YBVector(): size(0){
        memset(m_t, 0x0,  sizeof(T) * 100);
    }
    int push(T a){
        m_t[size] = a;
        size++;
        return size;
    }
    T get(int indx){
       return m_t[indx];
    }
    int get_size()  {
        return size;
    }
    void print(){
        for (int i=0; i < size;  ++i){
            std::cout <<  get(i) << std::endl;
        }

    }
private:
     T m_t[100];
    int size;
};
template<typename T>
YBVector<T> operator*(  YBVector<T>& lhs,  YBVector<T>& rhs ){
    YBVector<T> ret;
    for (int i=0; i < lhs.get_size();  ++i){
        ret.push(lhs.get(i) *  rhs.get(i)  );
    }
    return ret;

}
template<class T, size_t N>
class Array{
public:
    Array(){}
    Array(const initializer_list<T> & l)  {
        int i=0;
        for(const T & t: l){
            m_arr[i++] = t;
        }
    }
    T *getData() const{
        return m_arr;
    }
    void debug(){
        for(int i=0; i< N; ++i){
            cout << m_arr[i] << " ";
        }
    }
private:
    T m_arr[N];
};


void run_mylib1();

#endif
