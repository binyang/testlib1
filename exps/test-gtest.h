#ifndef __UT__H
#define __UT__H

struct BankAccount{
    int balance;
    explicit BankAccount(): balance(0){}
    explicit BankAccount(int b): balance(b){

    } 

   int deposit(int amount){
        balance += amount;
        return balance;
    }   
    bool withdraw(int  amount){
        if(balance >= amount){
            balance -= amount;
            return true;
        }
        return false;
    }


};

void run_test();
#endif