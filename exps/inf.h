
#ifndef __INF__

#define __INF__
class Inf{
    public:
    virtual void test()=0;
    virtual ~Inf(){}
};

#endif