
#ifndef __MYLIB2__

#define __MYLIB2__

#include <iostream>
#include "inf.h"

class Mylib2 : public Inf{
public:
    Mylib2(){
        std::cout << "Mylib2 constructor\n"; 
    }
    virtual ~Mylib2(){
        std::cout << "Mylib2 destructor\n"; 
    }
    virtual void test(){
        std::cout << "Mylib2 address:" << this <<std::endl;
    }
};



#endif