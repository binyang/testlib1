#include "test-boost.h"
#include <iostream>
#include <iterator>
#include <algorithm>
//#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/asio.hpp>

void test_boost_json(){
 try
    {
        std::stringstream ss;
        ss << "{ \"root\": { \"values\": [1, 2, 3, 4, 5 ] } }";

        boost::property_tree::ptree pt;
        boost::property_tree::read_json(ss, pt);

        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pt.get_child("root.values"))
        {
            assert(v.first.empty()); // array elements have no names
            std::cout << v.second.data() << std::endl;
        }
        
    }
    catch (std::exception const& e)
    {
        std::cerr << e.what() << std::endl;
    }
}

void test_boost_timer1(){
  std::cout<< "test timer, waiting 5 secs....";
  boost::asio::io_context io;
  boost::asio::steady_timer t(io, boost::asio::chrono::seconds(5));
  t.wait();

  std::cout << "Hello, world!" << std::endl;
}

void print(const boost::system::error_code& /*e*/)
{
  std::cout << "Hello, world!" << std::endl;
}

void test_boost_timer2(){
  std::cout<< "test timer, waiting 5 secs....";
  boost::asio::io_context io;
  boost::asio::steady_timer t(io, boost::asio::chrono::seconds(5));
  t.async_wait(&print);
  std::cout <<"main thread is waiting for any process\n";
  io.run();

  std::cout << "Hello, world!" << std::endl;
}


void run_boost_test(){
    //test_boost_json();
    //test_boost_timer1();
    test_boost_timer2();
}