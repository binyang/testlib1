


#include <stdio.h>

#include <vector>
#include <thread>
#include <chrono>
#include "mylib1.h"

/*
void* operator new(size_t size){
    cout << "malloc memory size:" << size <<endl;
    return malloc(size);

} 

void  operator delete(void* __p, std::size_t __sz) _NOEXCEPT
{
    cout << "release memory size:" << __sz <<endl;
    free(__p);
}
*/

struct Obj{

int x;

};

    void operator delete(void* ptr, std::size_t sz) 
    { 
        cout << "custom delete for size " << sz <<endl; 
        delete (ptr); // ::operator delete(ptr) can also be used 
    } 
    void operator delete[](void* ptr, std::size_t sz) 
    { 
        cout << "custom delete for size " << sz <<endl; 
        delete (ptr); // ::operator delete(ptr) can also be used 
    } 

void run_memory_test(){
    int *  p = new int(0);
    ::delete p;
}

template <typename T>
void templated_fn(T) {}

void run_ut(){

    auto l =  {1, 2, 4, 11};
    YBVector<int> v1(l);
    YBVector<int> v2 =  square<YBVector<int>>(v1);
    v2.print();

    std::initializer_list<int> il={1,3,3,6};
    templated_fn<int>(10);
    templated_fn< std::initializer_list<int>>(   il );
}

void run_mylib1(){
    /*char pbuff[32];
    for(int i=0; i < 10; ++i){
        sprintf(pbuff, "timer %d", i);
        Timer tm1(pbuff);
    }*/

    //run_memory_test();
    Array<int, 5> arr{1,2,3,4,5};
    arr.debug();

    

}


