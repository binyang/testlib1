
#include <gtest/gtest.h>
#include "test-gtest.h"
#include "mylib1.h"
#include <iostream>

struct BankAccountTest: testing::Test{
    BankAccount *  pBankAccount = nullptr;
    BankAccountTest(){
        std::cout <<"call constructor\n";
        pBankAccount = new BankAccount;
    }

    ~BankAccountTest(){
        std::cout <<"call destructor\n";
        delete pBankAccount;
    }
 
};

TEST_F(BankAccountTest, CanDeposit){
    std::cout << "call CanDeposit:" << pBankAccount<< std::endl;
    pBankAccount->deposit(100);
    EXPECT_EQ(100,  pBankAccount->balance);
}


TEST_F(BankAccountTest, BankAccountEmpty){
    std::cout <<"call BankAccountEmpty" <<  pBankAccount<< std::endl;
    EXPECT_EQ(0,  pBankAccount->balance);
}

/*
struct BankState{
    int  initial;
    int  deposit;
    int  withdraw;
    bool success;
};

struct BankAccountTest2 : testing::WithParamInterface<BankState>{
    BankAccount *  pBankAccount = new BankAccount;
   BankAccountTest2(){
       pBankAccount-> balance = GetParam().initial;
   }
};

TEST_P(BankAccountTest2, FinalBalance){
    pBankAccount->balance = GetParam().initial;
    pBankAccount->deposit(GetParam().deposit);
    bool success = pBankAccount->withdraw(GetParam().withdraw);
    EXPECT_EQ( GetParam().success, success);
    //EXPECT_EQ( pBankAccount->balance, );

}
/*
INSTANTIATE_TEST_CASE_P(Default, BankAccountTest2, testing::Values(
    BankState{1, 1, 1, true}
    
));

*/

struct MyLibTest : testing::Test{
    Mylib1 *myLib1 = nullptr;

    MyLibTest(){
        myLib1 = new Mylib1();
    }

    ~MyLibTest(){
        delete myLib1;
    }

};

TEST_F(MyLibTest,  testGet){
   ASSERT_EQ(100, myLib1->get(100));

}

void run_test(){
   testing::InitGoogleTest();

  
RUN_ALL_TESTS();   
}
