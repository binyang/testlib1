#include <stdio.h>
#include <dlfcn.h>
#include "foo.h"

int main(void)
{
    puts("This is a shared library test...");
    //foo();
	
   void *lib_handle;
   void (*fn)(int *);
   int x;
   char *error;

   lib_handle = dlopen("./libfoo.so", RTLD_LAZY);
   if (!lib_handle) 
   {
      fprintf(stderr, "%s\n", dlerror());
      exit(1);
   }

char *func[]={"ctest1", "ctest2"};
for(int i=0; i< 2;++i){
   fn = dlsym(lib_handle, func[i]);
   if ((error = dlerror()) != NULL)  
   {
      fprintf(stderr, "%s\n", error);
      exit(1);
   }

   (*fn)(&x);
   printf("Valx=%d\n",x);
}

   dlclose(lib_handle);
    return 0;
}
