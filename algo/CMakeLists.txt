cmake_minimum_required(VERSION 3.10)

add_compile_options(-Wall -Wextra -Wpedantic) 
set( SOURCE  main.cpp BinTree.cpp MyList.cpp Sort.cpp Buffer.cpp leetcode.cpp asio-server.cpp)


add_executable(algo ${SOURCE} )

target_link_libraries(algo implv1 ) 
target_include_directories(algo PUBLIC
                          "${PROJECT_BINARY_DIR}"
                          "${PROJECT_SOURCE_DIR}/lib"
                          "/usr/local/include"
                           )