
#include "MyList.h"

#include <iostream>
using namespace  std;


ListMgr::ListMgr(const  vector<int>& arr):phead(nullptr){
    phead = new Mylist(arr[0] );
    Mylist * pcurr = phead;
    for(int i = 1;  i < arr.size(); ++i){
      Mylist *pNode = new Mylist(arr[i] );
      pcurr->pnext = pNode;
      pcurr = pNode;
      pcurr->pnext = nullptr;
    }
  
}
void ListMgr::debug(){
     Mylist * pcurr = phead;

     while (pcurr != nullptr){
       cout << pcurr->val << " ";
       pcurr = pcurr->pnext;
     }

  }
  
ListMgr::~ListMgr(){
      Mylist * pcurr = phead;
      cout << "destructor " ;
     while (pcurr != nullptr){
       cout << pcurr->val << " ";
       Mylist * pnext = pcurr->pnext;
       delete pcurr;
       pcurr = pnext;
     }   
  }
  
void ListMgr::reverse(){
  if(phead->pnext == nullptr)
    return;
  Mylist * plast = phead;
  Mylist * pcurr = phead->pnext;
  plast->pnext = nullptr;
  while(pcurr->pnext != nullptr){
    Mylist * pnext = pcurr->pnext;
    pcurr->pnext=plast;
    plast = pcurr;
    pcurr = pnext;
  }
  pcurr->pnext = plast;
  phead = pcurr;

}



  void run_mylist(){
    ListMgr  mgr( {2, 5, 1, 3, 7}  );
    mgr.debug();
    cout << "------------------\n";
    mgr.reverse();
    mgr.debug();

  }