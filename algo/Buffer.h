#ifndef __BUFFER__
#define __BUFFER__

class CircularBuffer{
public:
    CircularBuffer(int n);
    ~CircularBuffer();

    CircularBuffer(const CircularBuffer & other ) ;
    CircularBuffer & operator=( const CircularBuffer & other ) ;


    bool enque(int * pbuff, int size);
    bool deque(int* pbuff, int size );
    int size() const;
private:
    int  m_capacity;
    int *m_pbuffer;
    int m_size;
    int m_readIndx;
    int m_writeIndx;
};

void run_buffer_test();

#endif
