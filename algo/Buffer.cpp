#include <vector>
#include <iostream>
#include <vector> 
#include "Buffer.h"


CircularBuffer::CircularBuffer(int n):m_capacity(n), m_pbuffer(new int[n]), 
    m_readIndx(0), m_writeIndx(0), m_size(0)
  {
  }

  CircularBuffer::~CircularBuffer(){
    delete [] m_pbuffer;
  }

CircularBuffer::CircularBuffer(const CircularBuffer & other ){
  if(this != &other){
    delete [] m_pbuffer;
    
    m_capacity = other.m_capacity;
    m_size = other.m_size;
    m_readIndx = other.m_readIndx;
    m_writeIndx = other.m_writeIndx;
    m_pbuffer = new int[m_capacity];
    memcpy(m_pbuffer, other.m_pbuffer, m_capacity);
  }
}

CircularBuffer & CircularBuffer::operator=( const CircularBuffer & other ){
  if(this != &other){
    delete [] m_pbuffer;
    
    m_capacity = other.m_capacity;
    m_size = other.m_size;
    m_readIndx = other.m_readIndx;
    m_writeIndx = other.m_writeIndx;
    m_pbuffer = new int[m_capacity];
    memcpy(m_pbuffer, other.m_pbuffer, m_capacity);
  }
  return *this;
}




  bool CircularBuffer::enque(int * pbuff, int size){
    if (size > m_capacity - m_size){
      return false;
    }
    if(m_writeIndx >= m_readIndx ){
      int nleft = m_capacity - m_writeIndx ;
      if ( nleft >= size){
        memcpy( m_pbuffer+m_writeIndx, pbuff, size *  sizeof(int) );
        m_writeIndx +=size;
        std::cout <<"m_writeIndx: "<< m_writeIndx << std::endl;
      }else{
        memcpy(m_pbuffer+m_writeIndx, pbuff, nleft * sizeof(int) );
        memcpy(m_pbuffer, pbuff+nleft, (size - nleft) * sizeof(int) );
        m_writeIndx = size - nleft;
      }
    }else{
        memcpy( m_pbuffer+m_writeIndx, pbuff, size  * sizeof(int) );
        m_writeIndx +=size;
      }
    m_size+= size;
  }

bool CircularBuffer::deque(int* pbuff, int size ){
    if (size >  m_size){
      return false;
    }  
    if(m_writeIndx >= m_readIndx ){
      memcpy(pbuff, m_pbuffer + m_readIndx, size * sizeof(int) );
      m_readIndx += size;
    }else{
      int ntail = m_capacity- m_readIndx ;
      if(ntail >= size){
        memcpy(pbuff, m_pbuffer + m_readIndx, size * sizeof(int) );
        m_readIndx += size;        
      }else{
        memcpy(pbuff, m_pbuffer + m_readIndx, ntail * sizeof(int) );
        memcpy(pbuff+ntail, m_pbuffer, (size-ntail) * sizeof(int) );
        m_readIndx=size-ntail;          
      }
    }    
    m_size -= size;
}
int CircularBuffer::size() const{
  return m_size;
}


void run_buffer_test(){
  CircularBuffer cb(6);

  int arr[] = {1,2,3};
  cb.enque(arr, 3);

  int ret[3];
  cb.deque(ret, 3);

  int arr2[3]={4,5,6};
cb.enque(arr2, 3);
cb.enque(arr, 3);

cb.deque(ret, 3);
cb.deque(ret, 3);

  for(int i=0; i < sizeof(ret)/ sizeof(int); ++i){
    std::cout << ret[i]<< " ";
  }
  std::cout << "\n";
}

