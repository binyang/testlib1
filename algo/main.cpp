#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <memory>
#include <stack>
#include "utils.h"

using namespace std;

  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 };


TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
  if( root1 == nullptr && root2 == nullptr){
    return nullptr;
  }else if(root1 == nullptr && root2 != nullptr){
    TreeNode* root = new TreeNode(root2->val );
    root->left = mergeTrees(nullptr, root2->left);
    root->right = mergeTrees(nullptr, root2->right);    
    return root;
  }else if(root1 != nullptr && root2 == nullptr) {
    TreeNode* root = new TreeNode(root1->val );
    root->left = mergeTrees(root1->left, nullptr);
    root->right = mergeTrees(root1->right, nullptr);    
    return root;
  }
  
  TreeNode* root = new TreeNode(root1->val + root2->val );
  root->left = mergeTrees(root1->left, root2->left);
  root->right = mergeTrees(root1->right, root2->right);
  return root;
}


bool isPar(string s){
  int low =0;
  int high=s.size()-1;
  while(low < high && s[low] == s[high]){
    low++;
    high--;
  }
  return low >= high;

}



class Iterator;
class Set{


class Myexception : public exception{
  
};

class Iterator
{
  public:
  Iterator(Set& s  ): m_set(s){

  }
  Iterator(const Iterator & itr  ): m_set(itr.m_set), m_curr(itr.m_curr){

  }
  char* operator->(){
    if (m_curr < m_set.m_size){
      return m_set.m_pdata+m_curr;
    }else 
      return nullptr;  
  }
  char operator*(){
    if (m_curr < m_set.m_size){
      return *(m_set.m_pdata+m_curr);
    }else 
      return '\0';  
  }
  Iterator & operator++(){
    if (m_curr < m_set.m_size){
        m_curr++;
        return *this;
    }else 
      throw Myexception();
  }

  Iterator  operator++(int){
    if (m_curr < m_set.m_size){
        
        m_curr++;
        return Iterator(*this);
    }else 
      throw Myexception();
  }
  private:
    int m_curr;
    Set&  m_set;
};

public:  






  typedef  Set::Iterator Iterator;
  Set(int size): m_size(size), m_pdata( new char[size]){
    for(int i=0; i < size; i++){
      m_pdata[i] = 'a'+i;
    }
  }
  ~Set(){
    delete [] m_pdata;
  }
  char * m_pdata;
  int m_size;
};

class A{
public:
A(){
}
~A(){
}  
};


bool search(vector<int> &v, int low, int high, int x){
  if ( low > high){
    return false;
  }
  if( low == high){
    return v[low] == x;
  }

  int mid = (low+high)/2;
  if( v[mid] == x){
    return true;
  }else if( v[mid] > x){
    return search(v, low, mid, x);
  }else{
    return search(v, mid, high, x);
  }
}

void triplet(vector<int> & v){
  if(v.size()<=2)
    return;
  sort(v.begin(), v.end(), [](int a, int b){ return a < b;});
  for(int i=0; i <v.size()-2 ; ++i){
    for(int j=i+1;  j < v.size()-1; ++j){
      int x = v[i] + v[j];
      if(search(v, j+1, v.size()-1, x )){
        cout << v[i] << " " << v[j] <<"=" << x<<endl;
      }
    }

  }

}

int submax(vector<int> & v){

  vector<int> cache(v.size());
  cache[0]=v[0];
  int max=v[0];
  for(int i =1 ; i< v.size(); ++i){
    if(cache[i-1] > 0){
      cache[i] = cache[i-1] + v[i];
      cout<< cache[i] << endl;
    }else{
      cache[i] = v[i];
    }
    if(cache[i]> max){
      max = cache[i];
    }

  }
  return max;

}

int pivot(vector<int> &v){
  if(v.size()==1)
    return 0;
  if(v.size()==2)
    return -1;

  int mid=v.size()/2;
  int sum1=0, sum2=0;
  for(int i=0; i < mid; ++i){
    sum1+=v[i];
  }
  for(int i=mid+1; i < v.size(); ++i){
    sum2+=v[i];
  }

  if(sum1 == sum2) 
    return mid;
  if(sum1 > sum2){
    for(int i=mid-1; i>0; --i){
      sum1 -= v[i];
      sum2 += v[mid];
      mid=i;
      cout<< "sum1:" << sum1 <<";sum2" << sum2 <<endl;
      cout << "mid:" << mid <<endl;
      if(sum1==sum2) 
        break;
    }
  }else{
    for(int i=mid+1; i<v.size()-1; ++i){
      sum1 += v[i];
      sum2 -= v[mid];
      mid++;
      cout << "mid2:" << mid <<endl;
    }
  }  
  return mid;

}

class MyException : public exception{

};
#include <string.h>
template< typename T>
class Queue{
public:
  Queue(int size):m_capacity(size), m_rdIdx(0), m_wrIdx(0), m_arr(new T[size]){

  }
  ~Queue(){
    delete [] m_arr;
  }
  Queue(const Queue & q){
    if(this != &q){
      m_rdIdx = q.m_rdIdx;
      m_wrIdx =q.m_wrIdx;
      m_capacity = q.m_capacity;
      m_arr = new T[m_capacity];
      memcpy(m_arr, q.m_arr, m_capacity);
      }
  }
  Queue & operator=(const Queue & q){
    if(this != &q){
        delete[] m_arr;
        m_rdIdx = q.m_rdIdx;
        m_wrIdx =q.m_wrIdx;
        m_capacity = m_capacity;
        m_arr = new T[m_capacity];
      }
      return *this;
  }

  void push(T t){
    if(m_wrIdx < m_capacity){
      m_arr[m_wrIdx] = t;
      m_wrIdx++;
    }else{
      if(m_rdIdx >0){
        memmove(m_arr, m_arr+m_rdIdx, m_wrIdx - m_rdIdx);
        m_rdIdx =0;
        m_wrIdx = m_wrIdx - m_rdIdx;
        m_arr[m_wrIdx] = t;
        m_wrIdx++;
      }else{
        throw MyException();
      }
    }
  } 
  T & pop(){
    if(size() > 0){
      T & t =m_arr[m_rdIdx];
      m_rdIdx++;
      cout <<" now rd:" <<m_rdIdx<< endl;
      return t;
    }else{
      throw MyException();
    }
  }
  bool empty(){
    return size()==0;
  } 

  int size(){
    cout <<"m_wrIdx - m_rdIdx:" << m_wrIdx <<":" << m_rdIdx <<endl;
    return m_wrIdx - m_rdIdx;
  }
private:
  int m_rdIdx;
  int m_wrIdx;
  int m_capacity;
  T *m_arr;
};

bool isExists(unordered_set<string>  & set, string subs){
  return set.find(subs) != set.end();
}
int deleteLet(unordered_set<string>  & set, string s){
  int maxLen=1;
  for(int i=0; i < s.size()-1; ++i){
    for(int j=i+1; j < s.size(); ++j){
      cout << "substr:" <<  s.substr(i, j-i) <<endl;
      if(isExists(set, s.substr(i, j-i)) ){
        if(j-i > maxLen){
          maxLen=j-i;
        }
      }
    }
  }
  return s.size() - maxLen;
}

  vector<TreeNode*> findAncetor(TreeNode* root, TreeNode* pNode, vector<TreeNode*> v){
  if(root== nullptr)
    return {};
  v.push_back(root);  
  if(root->val == pNode->val)
    return v;
  vector<TreeNode*> r = findAncetor(root->left, pNode, v);  
  if(r.size()>0)
    return r;
  else  
    return findAncetor(root->right, pNode, v);  
}


TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {

  vector<TreeNode*> pv = findAncetor(root, p, {});
  vector<TreeNode*> qv = findAncetor(root, q, {});
  
  for(int i=pv.size()-1; i>=0; --i){
    for(int j=qv.size()-1; j>=0; --j){
      if(pv[i]->val == qv[j]->val)
        return pv[i];
    }
  }
  return nullptr;
}
 struct ListNode {
     int val;
     ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
 };
 

ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
  unordered_set<ListNode*> set;

  while (headA != nullptr){
    set.insert(headA);
    headA= headA->next;
  }
  while (headB != nullptr){
    if(set.find(headB) != set.end()){
      return headB;
    }
    headB= headB->next;
  }
  return nullptr;
}

//1.001.1
void convert(string ver, vector<string> &v){
  const char*porig = ver.c_str();
  const char*pstr = ver.c_str();
  const char *pnew ;
  while( (pnew = strchr(pstr, '.')) != nullptr){
    v.push_back(ver.substr(pstr-porig, pnew-pstr));
    pstr=pnew+1;
  }
  if(pstr != nullptr)
    v.push_back(pstr);
}

int majorityElement(vector<int>& nums) {
  unordered_map<int, int> cache;
  unordered_map<int, int>::iterator itr;
  for(int e: nums){
    itr = cache.find(e);
    if( itr != cache.end()){
      itr->second++;
    }else{
      cache[e] =1;
    }
  }
  int halfCount = nums.size()/2;
  for(const pair<int, int> & p : cache){
    if(p.second > halfCount ){
      return p.first;
    }
  }
  return nums[0];

}

int findPeakElement(vector<int>& nums) {
  if(nums.size() == 1)
    return nums[0];
  if(nums[0] > nums[1])
    return 0;
  if(nums[ nums.size()-1] > nums[ nums.size()-2]  )
    return nums.size()-1;

  for(int i=1; i < nums.size()-1; ++i){
    if(nums[i] > nums[i-1] && nums[i] > nums[i+1]){
      return i;
    }
  }
  return 0;
}


//(2*(3-(4*5)))
bool isOprand(char ch){
  return ( ch == '(' || ch == '+' || ch == '-' || ch == '*' || ch == '/');
}
int popEx( stack<char> & st1,   stack<int>& st2){
  int val=st2.top(); st2.pop();
  while(st1.top() != '('){
    int v1 = st2.top(); st2.pop();
    char opr = st1.top(); st1.pop();
    switch(opr){
      case '+':
        val += v1;
        break;
      case '-':
        val -= v1;
        break;
      case '*':
        val *= v1;
        break;
      case '/':
        val /= v1;
        break;
    }
  }
  return val;
}

int compute(string s){
  stack<char> st1;
  stack<int> st2;
  int val=0;
  for(char ch: s){
    if(isOprand(ch)){
      st1.push(ch);
    }else if ( '0' <= ch && ch <='9'){
      st2.push(ch-'0');
    }else if( ch == ')'){
      val+=popEx(st1, st2);
    }
  }
  return val;
}

extern int asio_server(int argc, char * argv[]);

#include "test-linux.h"

#include <semaphore.h>
#include <unistd.h>

int main(int argc, char * argv[]){

}
