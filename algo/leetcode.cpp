#include <vector>
#include <iostream>
#include <vector> 
#include <unordered_map>
#include <string>
#include <algorithm>
#include <stack>
#include "leetcode.h"

using namespace  std;


vector<int> findTwo(vector< vector<int> > & vv, int left, int right, int target) {

    vector<int> v;
    int i=left;
    int j=right;
    while(i < j ){
      if(vv[i][0] + vv[j][0] > target){
        j--;
      }else if(vv[i][0] + vv[j][0] < target){
        i++;
      }else {
        v.push_back( vv[i][0] );
        v.push_back( vv[j][0] );
        i++;
        j--;
      }
    }
    return v;
  }

vector<vector<int>> threeSum(vector<int>& nums) {
    unordered_map<string, int> cache;
    char pbuff[256];
    vector< vector<int>  > vv;
    for(int k=0;k< nums.size(); ++k){
      vector<int> tmpV{ nums[k], k };
      vv.push_back(tmpV);
    }

    std::sort(vv.begin(), vv.end(), [](vector<int> & a, vector<int> & b){ 
        return a[0] < b[0];
      } );
  int size =  nums.size();
  vector< vector<int> >  ret;
  int i=0;
  while(i < size -2 ){
    vector<int> findV = findTwo(vv, i+1, size-1,   -vv[i][0]);
    if( findV.size() > 0 ){
        for(int k=0; k < findV.size() -1 ; k+=2){
          sprintf(pbuff, "%d-%d-%d", vv[i][0],  findV[k], findV[k+1]  );
          if(cache.find(string(pbuff)) == cache.end()){        
            ret.push_back( { vv[i][0],  findV[k], findV[k+1] } );
            cache[string(pbuff)] = true; 
          }
        }
    }
    i++;
  }
  return ret;

}

void test_3sum(){
  vector<int> input{-1,0,1,2,-1,-4};
  vector<vector<int>> vv=threeSum(input);
  for(vector<int> & v: vv){
    for(int i : v){
      cout << i<<" ";
    }
     cout <<"\n";
  }
}

  struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };

  ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
      vector<int> v1, v2, v;
      while(l1 != nullptr){
        v1.push_back(l1->val);
        l1 = l1->next;
      }    

      while(l2 != nullptr){
        v2.push_back(l2->val);
        l2 = l2->next;
      }

      int over=0;
      int size1=v1.size();
      int size2=v2.size();
      int k=0;
      while(k < size1 && k < size2){
        int r = v1[k] + v2[k] + over;
        if(r>= 10){
          v.push_back(r-10);
          over=1;
        }else{
          v.push_back(r);
          over=0;
        }
        k++;
      }

      while(k < size1 && k >= size2 ){
          int r = v1[k]  + over;
          if(r>= 10){
            v.push_back(r-10);
            over=1;
          }else{
            v.push_back(r);
            over=0;
          }
          k++;
      }


      while(k< size2 && k >= size1 ){
          int r = v2[k] + over;
          if(r>= 10){
            v.push_back(r-10);
            over=1;
          }else{
            v.push_back(r);
            over=0;
          }
          k++;
      } 
      
      if(over ==1){
        v.push_back(1);
      }

      ListNode * phead, *pcurr;
      phead = new  ListNode(v[0]);
      pcurr= phead;
      for( int k=1; k< v.size(); ++k){
        ListNode *  ptmp = new  ListNode(v[k]);
        pcurr->next = ptmp;
        pcurr=ptmp;
      }
      pcurr->next = nullptr;
      return phead;

  }


    int reverse(int x) {
      bool positive = true;
      unsigned int MAX = std::pow(2, 21);
      if(x < 0){
        x = -x ;
        positive = false;
      }

      vector<int> v;
      while(x >= 10){
        int d = x - 10*( x/10);
        v.push_back(d);
        x = x/10;
      }
      v.push_back(x);

      unsigned int ret=0;
      for(int e: v){
        if(ret * 10 > MAX) return 0;
        ret = ret * 10 + e;
      }
      return positive? ret : -ret;
    }


  int lengthOfLongestSubstring(string s) {
    if(s.size()==0 ||  s.size() == 1) 
      return s.size();
    int max=1;
    unordered_map<char, bool> cache;
    for(int i=0; i < s.length()-1; ++i){
      int j=i+1;
      cache.clear();
      cache[ s[i]] = true;
      while(j< s.length() &&  (cache.find( s[j] ) == cache.end()) ){
          cache[ s[j] ] = true;
          j++;
      }
      if (j-i > max ){
        max = j - i;
      }
      
    }    
    return max;
  }

  int Palindrome(string & s, int pivot){
    int left = pivot -1 ; 
    int right = pivot +1;
    while(left >0 && right < s.size()){
      if( s[left] != s[right])
        break;
      left--;
      right++;
    }
    return right - left;
  }

  string longestPalindrome(string s) {
    if(s.size()==0 || s.size()==1)
      return s;
    int max_pivot =1 ;
    int max_len = 1;
    for(int i=1; i< s.size()-1 ; ++i){
      int currLen = Palindrome(s, i);
      if(currLen > max_len){
        max_len = currLen;
        max_pivot = i;
      }
    }
    int off = (max_len - 1)/2;
    return s.substr( max_pivot - off,  max_pivot + off);

  }

  int myAtoi(string s) {
    int i=0; 
    while(i < s.size() && !(s[i] >='1' && s[i] <='9' || s[i] == '-')  ){
        i++;
    }
    if (i> 0)
    return 0;
    int positive = true;
    if(s[i] == '-'){
      positive = false;
      i++;
    } 
    int j=s.size()-1;
    while(j >= i &&  ! (s[j] >='1' && s[j] <='9')){
        j--;
    }
    if(j==i)
      return 0;
    int k = i;
    int ret =0;
    int digit;
    while (k <=j){
      digit = s[k] - '1' +1;
      ret = ret*10 + digit;
      k++;
    }  
    return positive?ret: -ret;
  }



  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };
 
    vector<int> inorderTraversal(TreeNode* root) {
        if (root == nullptr)
          return {};
         vector<int> leftArr = inorderTraversal(root->left);
         leftArr.push_back(root->val);
         vector<int> rightArr = inorderTraversal(root->right);
         leftArr.insert(leftArr.end(), rightArr.begin(), rightArr.end());
         return leftArr;
    }


  bool isSameTree(TreeNode* p, TreeNode* q) {
      if(p == nullptr && q == nullptr){
        return true;
      }else if( p != nullptr && q != nullptr){
        return  (p->val == q->val) &&  
          isSameTree (p->left, q->left) && 
            isSameTree (p->right, q->right );
      }else{
        return false;
      }
  }

/*
  bool isValidBST(TreeNode* root, int min) {
    if(root == nullptr )
      return true;
    if(root->left !=  nullptr && root->right !=  nullptr){
      if( root->left->val >= root->val || root->right->val <= root->val )
      { 
        return false;
      }else{
        return  isValidBST(root->left) && isValidBST(root->right);
      }
    }else if(root->left !=  nullptr){
      if( root->left->val >= root->val)
      { 
        return false;
      }else{
        return  isValidBST(root->left) ;
      }
    }else if(root->right !=  nullptr){
      if( root->right->val <= root->val)
      { 
        return false;
      }else{
        return  isValidBST(root->right) ;
      }

    }else{
      return true;
    }
  
  }

    bool isValidBST(TreeNode* root) {
      if(root == nullptr)
        return true;
      return isValidBST(root, root->val);  

    }
    */

    int uniquePaths(int m, int n) {
      if(m==1 ||  n== 1)
        return 1;
      return uniquePaths(m-1, n) + uniquePaths(m, n-1);      


    }



void dumpToStatck(const string & s, stack<char> t){
  for(char c: s)
    t.push(c);
}


string addBinary(string a, string b) {
  stack<char> st1, st2, st3;
  for(char c: a)
    st1.push(c);  

  for(char c: b)
    st2.push(c);  

  int over=0;
  int i1, i2, digit;
  while( !st1.empty() && !st2.empty() ){
    i1=st1.top()-'0';
    i2=st2.top()-'0';
    digit = (i1 + i2 + over) % 2;
    if( (i1 + i2 + over) > 1){
      over=1;
    }else{
      over =0;
    }
    st3.push('0'+ digit);

    st1.pop();
    st2.pop();
  }

  while( !st1.empty() ){
    i1=st1.top()-'0';
    digit = (i1 +  over) % 2;
    if( (i1 + over) > 1){
      over=1;
    }else{
      over =0;
    }
    st3.push('0'+ digit);
    st1.pop();
  }

  while( !st2.empty() ){
    i2=st2.top()-'0';
    digit = (i2 +  over) % 2;
    if( (i2 + over) > 1){
      over=1;
    }else{
      over =0;
    }
    st3.push('0'+ digit);
    st2.pop();
  }
  if(over==1){
    st3.push('1');
  }


  string s;
  while(!st3.empty()){
    cout <<"s3.top:" << st3.top() <<endl;
    s+=st3.top(); 
    st3.pop();
  }
  return s;
}
#include <sstream>
/*
void spiltByDelimitors(const string & input, const std::string& delimitors, std::vector<std::string>& out){
    std::stringstream inputstrm(input);
    std::string segment;
    out.clear();
    for( char s : delimitors){
        std::getline(inputstrm, segment, s);
        if(segment.size() > 0)
            out.push_back(segment);
    }
}
*/
void split(const string& input, const string & delimitors, vector<string> & out){
  std::stringstream ss(input);
  string o;
  for(char  deli : delimitors){
    std::getline( ss, o, deli);
    if(o.size() > 0){
      out.push_back(o);
    }
  }
}


#include <string.h>
#include <stdlib.h>

void parHTML( const string & s, string &tag, unordered_map<string, string>& map){
int i=0;
int j = s.size()-1;
char * pbuff = new char[s.size()+1 ] ;
memcpy(pbuff, s.c_str(),s.size() );
pbuff[s.size()] ='\0';
if(pbuff[j] == '>')
  j--;

if(pbuff[i] == '<')
  i++;

string skey, sval;
char *newPtr = strchr(pbuff+i, ' ');
int newPos = newPtr-pbuff;
char key[256], val[256];
memset(key, 0x0, 256);
strncpy(key, pbuff+i,   newPos-i);
tag=key;
i = newPos+1;

while (i < j){
  newPtr = strchr(pbuff+i, '=');
  if(newPtr == nullptr)
    break;
  newPos = newPtr-pbuff;
  
  memset(key, 0x0, 256);
  strncpy(key, pbuff+i,  newPos-i );
  
  i = newPos+1+1;
  newPtr = strchr(pbuff+i, ' ');
  if(newPtr == nullptr)
    break;
  newPos = newPtr-pbuff;
  memset(val, 0x0, 256);
  if( *(pbuff+i) == '\"')
    i++;
  strncpy(val, pbuff+i,  newPos-i-1 );
  i = newPos +1;
  map[skey] = sval;
}
memset(val, 0x0, 256);
if( *(pbuff+i) == '\"')
  i++;
strncpy(val, pbuff+i, j-i);

map[key] = val;
delete[] pbuff;

}

void parseHRML(){
  int lineNum, queryNum;
  cin>>lineNum;
  cin>> queryNum;
  string line;
  string prefix="";
  unordered_map<string, unordered_map<string, string>> mm;
  for(int i=0; i < lineNum/2; ++i){
    cin>>line;

    string tag;
    unordered_map<string,string > map;

    parHTML(line, tag, map);
      prefix += tag;
    if(prefix.size() == 0)


    mm[prefix]= map;

  }
  unordered_map<string, unordered_map<string, string>>::iterator itr;
  unordered_map<string, string>::iterator itr2;
  for(itr= mm.begin(); itr != mm.end() ; ++itr)
    cout << itr->first <<endl;
    for(itr2= itr->second.begin(); itr2 != itr->second.end(); ++itr2 ){
      cout << itr2->first<<":" << itr2->second <<endl;
    }

}


template<typename T>
class AddElements{
public:    
  AddElements(const T& input):a(input){

  }
  ~AddElements(){
    delete pbuff;
  }
  T add(T& b){
    return a + b;
  }
  char* concatenate(const T & b){
      pbuff = new char[a.size() + b.size() +1 ];
      memcpy(pbuff, a.c_str(), a.size());
      memcpy(pbuff+ a.size(), b.c_str(), b.size());
      return pbuff;
  }
private:
  T a;
  char *pbuff; 
};


class Complex{
public:
  Complex & operator+(const Complex & c){
    a+=c.a;
    b+=c.b;
    return *this;
  }
  int a;
  int b;
};


Complex  operator+(const Complex & A, const Complex & B){
  Complex c;
  c.a = A.a + B.a;
  c.b = A.b + B.b;
  return c;
}

ostream & operator<<(ostream & o, const Complex &c){
  o<< c.a << "+i" << c.b;
  return o;
}

class Matrix {
public:
  Matrix(){}
  Matrix( vector<vector<int>> & iv ){
    v.insert(v.begin(), iv.begin(), iv.end());
  }

Matrix& operator+( const Matrix & b){
  int n=v.size();
  int m=v[0].size();
  for(int i=0; i < n; ++i){
    for(int j=0; j < m; ++j){
      v[i][j]  +=  b.v[i][j] ;
    }
  }
  return *this;
}

Matrix operator+=( const Matrix & b){
  int n=v.size();
  int m=v[0].size();
  for(int i=0; i < n; ++i){
    for(int j=0; j < m; ++j){
      v[i][j]  +=  b.v[i][j] ;
    }
  }
  return *this;
}


  void print(){
    int n=v.size();
    int m=v[0].size();    
      for(int i=0; i < n; ++i){
        for(int j=0; j < m; ++j){
          cout << v[i][j] << " ";
        }
        cout << std::endl;
      }
  }
private:
  vector<vector<int>> v;
};



Matrix * makeMatrix(int n, int m){
    int var;
    vector<vector<int>> vv;
    for(int i=0; i < n; ++i){
      vector<int> v;
      for(int j=0; j < m; ++j){
        cin >> var;
        cout << var <<endl;
        v.push_back( var );
      }
      vv.push_back(v);
    }
    return new Matrix(vv);
}

void test_Matrix(){
  int t;
  cin >> t;
  
  while(t>0){
    int n,m;
    cin >> n >> m;
    Matrix *pa, *pb;
    pa = makeMatrix(n,m);
    pb = makeMatrix(n,m);
    pa->operator+(*pb);
    pa->print();
    delete pa;
    delete pb;
    t--;
  }

}

struct Workshop{
  int id;
  int start_time;
  int duration;
  int end_time;
  Workshop(int idx, int start, int dur){
    id = idx; 
    start_time = start;
    duration = dur;
    end_time = start_time + dur;
  }
};

struct Available_Workshops{
  Available_Workshops(int n, int start_time[], int duration[] )  {
    for(int i=0;  i< n; ++i){
      Workshop * pws = new Workshop(i,start_time[i], duration[i]);
      arr.push_back(pws);
    }

  }
  ~Available_Workshops(){
    for( Workshop *p : arr){
      delete p;
    }
  }
  vector<Workshop*> arr;
};

Available_Workshops *initialize(int start_time[], int duration[], int n) {
  Available_Workshops * pAW = new Available_Workshops(n, start_time, duration);  
  return pAW; 
}


int CalculateMaxWorkshops(Available_Workshops* ptr) {
  std::sort(ptr->arr.begin(), ptr->arr.end(), [](Workshop*pa, Workshop*pb){
    return ( pa->end_time < pb->end_time);
  } );
  int total=1;
  int lastIdx=0;
  for(int i=1;  i < ptr->arr.size(); ++i){
    if( ptr->arr[lastIdx]->end_time <=  ptr->arr[i]->start_time){
      total ++;
      lastIdx = i;
    }

  }
  return total;

}

enum class Fruit { apple, orange, pear };
enum class Color { red, green, orange };

template <typename T>
struct Traits{
  static string name(int index){
    if(typeid(T) == typeid(Fruit)){
      switch (index){
        case 0:
          return "apple";
        case 1:
          return "orange";
        case 2:
          return "pear";  
        default:
          return "unknown";            
      }
    }else{
      switch (index){
        case 0:
          return "red";
        case 1:
          return "green";
        case 2:
          return "orange";
        default:
          return "unknown";  

      }
  }
  }
};


vector<int> longestIncSeq(vector<int>  v){
  vector<vector<int>> vv;
  vv.push_back( { v[0] });
  for(int i=1; i < v.size(); ++i){

    vector<vector<int>>tmpVV;

    for(int j=0; j< vv.size(); ++j){
      int size = vv[j].size();
      if(vv[j][size-1] < v[i] ){
        vector<int> tmp = vv[j];
        tmp.push_back(v[i]);
        tmpVV.push_back(tmp);
      }
    }
    vv.insert(vv.end(), tmpVV.begin(), tmpVV.end() );

  }
  int max=1;
  vector<int> maxV;
  for(int i=0; i < vv.size(); ++i){
    if(vv[i].size() > max){
      maxV = vv[i];
    }
  }
  return maxV;
}


vector<int> longestConsecSeq(vector<int> &v){
  std::sort(v.begin(), v.end(), [](int a, int b){ return a < b;} ) ;
  int i=0, j=1;
  int max=1;
  vector<int> ret;

  while (j < v.size() ){
    while(v[j]== (v[j-1] + 1)  && j < v.size()){
      //cout <<v[j] << ",";
      j++;
    }

      if(j<= v.size()){
      if( j-i > max){
        ret.clear();
        ret.insert(ret.begin(), v.begin()+i , v.begin()+j);
        max =j-i;
      }
    }
    i=j;
    j++;
  }
  return ret;
}


int knapsack(int w, const vector<int>&  weights, const vector<int>& values, int n){
  if(n==0){
    if(weights[0] <= w){
      return values[0];
    }else{
      return 0;
    }
  }
  int choice1 = knapsack(w - weights[n], weights, values, n-1 ) + values[n];
  int choice2 = knapsack(w,  weights, values, n-1 );
  return (choice1 > choice2)? choice1: choice2;
    
}

int min(int a, int b){
  return a<b?a:b;
}
int max(int a, int b){
  return a>b?a:b;
}

vector<vector<int>> merge(vector<vector<int>>& intervals) {

  std::sort(intervals.begin(),  intervals.end(), [](vector<int>&a,  vector<int>&b){
    return a[0] < b[0];
  } );

  vector<vector<int> > retVV;
  retVV.push_back( intervals[0] );
  for(int i=1; i < intervals.size() ; ++i){
    vector<int> &queV = retVV[ retVV.size()-1];
    if( queV[1] < intervals[i][0] ){
      retVV.push_back( intervals[i] );
    }else{
      int _min=min(queV[0], intervals[i][0]);
      int _max=max(queV[1], intervals[i][1]);
      queV[0]= _min;
      queV[1]= _max;

    }

  }
  return retVV;  
}





void run_leetcode(){

  

  
  

  /*vector<int> weights{40, 10,20,30};
  vector<int> values{1000, 60,100,120};
  cout << knapsack(50, weights, values, 3)<<endl;
*/


/*
  int num;
  cin >> num;
  string line, type, op1, op2;
  
  for(int i=0; i < num; ++i){
    stringstream ss(line);
    std::getline(ss, type, ' ');
    std::getline(ss, op1, ' ');
    std::getline(ss, op2);
    if(type =="string"){
        cout <<add<string>(op1, op2);
    }else if (type == "int"){
       cout << add<int> (atoi(op1.c_str()),  atoi(op2.c_str()) );
    }else if(type == "float"){
      cout << add<float> (atof(op1.c_str()),  atof(op2.c_str()) );
    }else if(type == "long"){
      cout << add<long> (atol(op1.c_str()),  atol(op2.c_str()) );
    }

  }
*/
  //test_3sum();
  //cout << std::log(1534236469)<<endl;
  //cout << reverse(1534236469);
  //cout<< lengthOfLongestSubstring("abcabcbb") << std::endl;
  //cout << longestPalindrome("cbbd")<<std::endl;
  //cout << longestPalindrome("a")<<std::endl;
  //cout << myAtoi("   -42")<< std::endl;
  //cout << uniquePaths(3,7) <<std::endl;
  //cout << addBinary("111", "10")<<endl;

/*  string s="<ab>hello</ab>";
  vector<string> vs;
  split(s, "<></>", vs);
  for(const string & s: vs){
    cout <<s <<endl;
    
  }
*/


/*
std::string s="<tag3 another = \"another\" final = \"final\">";
string tag;
unordered_map<string,string > map;
parHTML(s, tag, map);
cout<< "tag:" << tag<<endl;
for(auto p: map){
  cout << p.first << ":" << p.second <<endl;
}

//parseHRML();
*/



//test_Matrix();




}

