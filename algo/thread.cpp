
#include <iostream>
#include <pthread.h>

struct Args_t
{
  pthread_mutex_t *pmutex;
  int *pint;
};



void * func1(void*ptr){
  Args_t * pArgs= (Args_t*)ptr;
  
  pthread_mutex_lock(pArgs->pmutex );
  for(int i =0; i< 100; i++){
    int c = *(pArgs->pint);
    *(pArgs->pint) = c+1;
  }
  cout << *(pArgs->pint) <<endl;
  pthread_mutex_unlock( pArgs->pmutex);
}

int main(int argc, char * argv[]){

  pthread_t tid[10];
  int g_int=0;
  pthread_mutex_t mutex;
  pthread_mutex_init(&mutex, nullptr);

  Args_t args;
  args.pmutex = &mutex;
  args.pint = &g_int;


  for(int i=0; i< 10; ++i){
    pthread_create(&(tid[i]), nullptr, func1, &args );
  }

  for(int i=0; i< 10; ++i){
    pthread_join(tid[i], nullptr);
  }

  cout << "g_int:" << g_int <<endl;
}
