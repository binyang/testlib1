#include "Sort.h"
#include <vector>
#include <iostream>
#include <vector> 

void swap(int *parr,  int i, int j){
  int tmp = parr[i];
  parr[i]=parr[j];
  parr[j]=tmp;
}

void shift(int *parr, int i, int j){
  while(j != i){
    parr[j+1] = parr[j];
    j--;    
  }

}

void quick_sort1(int *parr, int low, int high  )
{
  std::cout <<"low:" << low << ";high:" << high <<std::endl;
  if(low > high)
    return ;
  int pivot = high;
  high--;

  int i=low;
  int j=low;

  while (j <= high){
    while(parr[j] > parr[pivot])
      j++;
    
    swap(parr, i, j);
    i++;
    j++;
  }
  swap(parr, i, pivot);
   std::cout <<"i:" << i << ";j:"<< j <<std::endl;
  //quick_sort1(parr, low,  i-1);
  //quick_sort1(parr, i+1, high);
}

void production(int *A , int *B, int size){
  int totN=1;
  std::vector<int> v;  
  for(int i=0; i < size ; ++i){
    if(A[i] == 0){
      v.push_back(i);
    }else{
      totN *= A[i];
    }
  }
  if(v.size()> 0){
    memset(B, 0x0, sizeof(int) * size);
    if(v.size() == 1){
      B[ v[0] ] = totN;
    }
  }else{
    for(int i=0; i< size; ++i){
      B[i] = totN/A[i];
    }
  }
}

/*void func(int , int){
  std::cout <<" call int\n";
}*/

class A{
 public: 
  virtual void f(){
          std::cout <<" A.f\n";
  }


};
class B : public A{
public:
    virtual void f(){
      std::cout <<" B.f\n";
    }
};


class CircularBuffer{
public:
  CircularBuffer(int n):m_capacity(n), m_pbuffer(new int[n]), 
    m_readIndx(0), m_writeIndx(0), m_size(0)
  {

  }
  ~CircularBuffer(){
    delete [] m_pbuffer;
  }
  bool enque(int * pbuff, int size){
    if (size > m_capacity - m_size){
      return false;
    }
    if(m_writeIndx >= m_readIndx ){
      int leftn = m_capacity - m_writeIndx ;
      if ( leftn >= size){
        memcpy( m_pbuffer+m_writeIndx, pbuff, size);
        m_writeIndx +=size;
      }else{
        memcpy(m_pbuffer+m_writeIndx, pbuff, leftn);
        memcpy(m_pbuffer, pbuff+leftn, size - leftn);
        m_writeIndx = size - leftn;
      }
    }else{
        memcpy( m_pbuffer+m_writeIndx, pbuff, size);
        m_writeIndx +=size;
      }
    m_size+= size;
  }
bool deque(int* pbuff, int size ){
    if (size >  m_size){
      return false;
    }  
    if(m_writeIndx >= m_readIndx ){
      memcpy(m_pbuffer + m_readIndx, pbuff, size);
      m_readIndx += size;
    }else{
      int ntail = m_capacity- m_readIndx ;
      if(ntail >= size){
        memcpy(m_pbuffer + m_readIndx, pbuff, size);
        m_readIndx += size;        
      }else{
        memcpy(m_pbuffer + m_readIndx, pbuff, ntail);
        memcpy(m_pbuffer, pbuff, size-ntail);
        m_readIndx=size-ntail;          
      }
    }    
    m_size -= size;
}
int size() const{
  return m_size;
}


private:
  int  m_capacity;
  int *m_pbuffer;
  int m_size;
  int m_readIndx;
  int m_writeIndx;
};




void run_sort_test(){

  int arr[]={ 2,1,3,0,5,0} ;
  std::cout << sizeof(int) << std::endl;
  std::cout << sizeof(int*)<< std::endl;
  /*quick_sort1(arr, 0, sizeof(arr) / sizeof(int) -1);
  for(int i=0; i < sizeof(arr) / sizeof(int); ++i){
    std::cout<<arr[i] <<" " ;
  }*/

  A *pA = new B();
  pA->f();


}

