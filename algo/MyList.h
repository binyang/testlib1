#ifndef __MYLIST__
#define __MYLIST__
#include <vector>
struct Mylist{
  Mylist(int _val):val(_val), pnext(nullptr){

  }
  int val;
  struct Mylist * pnext;
};


struct ListMgr{
  ListMgr(const std::vector<int>& arr);
 
  void debug();
  void reverse();
  ~ListMgr();
  Mylist * phead;

};

void run_mylist();

#endif
